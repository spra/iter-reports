\contentsline {chapter}{\numberline {1}\huge {Introduction}}{2}{chapter.1}%
\contentsline {chapter}{\numberline {2}\huge {Full MHD eddy current coupling}}{7}{chapter.2}%
\contentsline {section}{\numberline {2.1}The vacuum and wall response}{7}{section.2.1}%
\contentsline {section}{\numberline {2.2}Vacuum response tests}{13}{section.2.2}%
\contentsline {section}{\numberline {2.3}Benchmarking of the full MHD coupling}{15}{section.2.3}%
\contentsline {subsection}{\numberline {2.3.1}$n=0$ VDE}{15}{subsection.2.3.1}%
\contentsline {subsection}{\numberline {2.3.2}Asymmetric Tearing Mode}{15}{subsection.2.3.2}%
\contentsline {chapter}{\numberline {3}\huge {Halo current coupling}}{18}{chapter.3}%
\contentsline {section}{\numberline {3.1}The electromagnetic model}{18}{section.3.1}%
\contentsline {section}{\numberline {3.2}Methods for the solution of the generalized virtual current}{25}{section.3.2}%
\contentsline {subsection}{\numberline {3.2.1}The EM problem on the shell}{25}{subsection.3.2.1}%
\contentsline {subsection}{\numberline {3.2.2}Matrix form}{27}{subsection.3.2.2}%
\contentsline {subsubsection}{Test setup}{28}{subsubsection*.13}%
\contentsline {subsection}{\numberline {3.2.3}The "staggered" approach}{28}{subsection.3.2.3}%
\contentsline {subsubsection}{1. Null scheme}{30}{subsubsection*.16}%
\contentsline {subsubsection}{2. "Direct" scheme}{33}{subsubsection*.18}%
\contentsline {subsection}{\numberline {3.2.4}Biot-Savart approach with null scheme}{34}{subsection.3.2.4}%
\contentsline {subsection}{\numberline {3.2.5}Induction equation approach}{35}{subsection.3.2.5}%
\contentsline {subsubsection}{Block inversion}{35}{subsubsection*.19}%
\contentsline {subsubsection}{Null scheme}{36}{subsubsection*.20}%
\contentsline {section}{\numberline {3.3}Simulation of the extended virtual casing principle}{37}{section.3.3}%
\contentsline {subsection}{\numberline {3.3.1}Singular wire tests}{37}{subsection.3.3.1}%
\contentsline {subsection}{\numberline {3.3.2}Realistic wire tests}{41}{subsection.3.3.2}%
\contentsline {subsection}{\numberline {3.3.3}Bulk conductor tests}{44}{subsection.3.3.3}%
\contentsline {section}{\numberline {3.4}Conclusions and next steps}{47}{section.3.4}%
