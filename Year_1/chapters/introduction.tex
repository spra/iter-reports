Natural disruptions occurring in tokamak devices bring about a series of negative consequences on the plasma confinement and the machine's integrity. These events, originated by an excessive growth of plasma instabilities, determine a loss of thermal and magnetic energy stored within the plasma, which in turn leads to detrimental heat and mechanical loads on the surrounding components. \\
Vertical Displacement Events (VDEs) are among the most crucial disruptive scenarios for ITER-like tokamaks. As they consist in a vertical movement of the plasma, the magnetic field perturbations associated with these phenomena lead to a flow of electric currents in the vacuum vessel and adjacent components; these currents interact with the background magnetic field (prevalently toroidal) and may originate severe $\mathbf{J \times B}$ forces on the mechanical components. \\
Hence, the design of the machine's components is dictated by the order of magnitude of the maximum force that is expected to act on them. Simulations and predictions performed with numerical codes serve to accomplish this task. \\

\medskip
From a broad perspective, this project is focused on the generalization of free boundary coupling schemes modelling the electromagnetic plasma-wall interaction, which affects the dynamics of the plasma, the eddy currents in the conductive structures and the halo currents flowing between the plasma and these structures. \\
The extensive scenario of the wall current fields associated with a disruption is responsible for the onset of vertical and horizontal stresses. \\
While a good estimate of the maximum integral vertical force $F_Z$ can be obtained even with axisymmetric (2D) codes, the same is not true for the horizontal (or \emph{sideways}) force, which requires more computationally expensive 3D MHD codes. \\
The main purpose of the work is to attain a reliable prediction of the magnitude and rotation of the sideways force on the ITER vacuum vessel during unmitigated Asymmetric VDEs (also known as AVDEs).  

\medskip

Due to their rotation, sideways forces pose a severe threat to the integrity of the vacuum vessel in ITER, since they could oscillate in resonance with the mechanical eigenfrequenceis of the vessel itself, thus leading to their undesired amplification.
% Mention halo modelling developed through the years

% - V. Riccardo, source-sink model 2000
% - Gerasimov, JET measurement for sideways force ~4 MN 2015
% - Bachmann 2011, source-sink applied to ITER and found 40 MN
% - Pustovitov 2021, kink mode model interacting only via halos finds smaller force

\medskip

The principles driving the vertical force $F_Z$ are well understood, and a general consensus concerning the scale of the maximum integral vertical force in ITER has been reached (order of $80 \, MN$ \cite{clauser2019vertical}). However, the disruption community has not yet met an agreement regarding the suitable model to be adopted for an accurate calculation of the sideways force $F_h$. \\
Modelling studies for this kind of force have been leveraging experimental evidence in machines such as COMPASS and JET \cite{gerasimov2015jet}. In particular, the latter lends itself well for the study of AVDE scenarios, because the flexibility of the vessel system leads to sideways displacements of its structure when horizontal forces arise, thus allowing to perform an indirect measurement of said forces through these oscillations. \\
\newtildeON
A maximum value $F_{h,\emph{max}} \, ~ \,  4 \, MN$ has been found for the JET vacuum vessel. 

\medskip
Based on JET AVDE pulses, a source-sink analytical model \cite{riccardo2000forces} was formulated under certain simplifying assumptions;
%(only forces linear with perturbations, modes higher than $(1,1)$ excluded, approximation of certain plasma and vessel parameters)
the results obtained from its application to a simplified geometry suggest that the dominant contribution to the sideways force is determined by the asymmetric path of the shared (halo) wall current (and therefore, by the corresponding plasma current asymmetry $\delta I_p$) caused by the 1/1 MHD mode structure. \\ 
The aforementioned model also reveals that the estimated $F_h$ appears to be only slightly smaller than Noll's force \cite{noll1997present} which, treating the kinked plasma as a tilted rigid current-carrying filament, envisages a scaling of the force with the variation of the whole vertical current moment $\delta M_Z = z_p \delta I_p + \delta z_p I_p$, where $z_p$ is the $Z$ coordinate of the plasma current centroid. \\
Simulations of JET AVDEs recently carried out with the non-linear 3D MHD code JOREK \cite{artola2024modelling} lead to this conclusion as well: this point will be taken up and further discussed at the end of this paragraph. \\
Extrapolation of the sideways forces to ITER using Noll's formula as a conservative upper estimate \cite{gerasimov2010scaling} and dedicated studies using the aforementioned source-sink model \cite{bachmann2011specification} yield a maximum horizontal force in the order of $ ~ \, 40 \, MN$, that is 1 order of magnitude larger than the force in JET and represents an important concern for the structural integrity of the ITER vacuum vessel.\\
However, other models \cite{pustovitov2022models} treating the AVDE as a 1/1 kink mode in the stage before the plasma-wall contact (and therefore interacting only via eddy currents) find forces an order of magnitude smaller. \\
Because of their potentially detrimental nature, sideways forces' modelling and predictions must be first validated at ITER in low-plasma-current experiments before moving to higher $I_p$ operation. 

\section*{\normalsize Scope of this work}
The above-mentioned analytical models, not being fully self-consistent, assume that the kink perturbations in plasma current centroid position ($\delta z_p$) and toroidal current ($\delta I_p$) along the toroidal angle are the same for JET and ITER. \\
Since this assumption has not been properly validated, it is evident that non-linear MHD reproductions of ITER AVDEs are needed. \\
Hence, the JOREK simulations presented in \cite{artola2024modelling} have been carried out with this intent. Even though most of the JET experimental features were successfully reproduced by JOREK (e.\@g.\@ displacement $\delta z_{p_{n=1}}$, temporal evolution of $q_{95}$ and axisymmetric plasma current $I_p$) the computed sideways' force was about 1 order of magnitude smaller than the experimental value. \\
Concurrently, one of the limitations in the JOREK code is the enforcement of the following boundary condition on the plasma current:
\begin{equation}
\frac{\partial I_p}{\partial \phi} = 0
\end{equation}
which implies that, although asymmetric halo currents are included in the model, their treatment on the JOREK boundary envisages that they encounter an equipotential perfectly conductive surface on the outside, thus completely shielding their interaction with the vacuum and the surrounding conducting structures and preventing the injection/drain of current to/from the wall via source-sink potentials.

\medskip
This project is aimed at relaxing the $I_p$-symmetry constraint through a self-consistent coupling of halo currents between JOREK \cite{hoelzl2021jorek} and the electromagnetic code CARIDDI \cite{albanese1988integral} \cite{isernia2023self} for the modelling of conducting structures. \\
In parallel, development activities have been directed towards the completion and validation of the eddy current coupling of full MHD JOREK with wall codes, which could be necessary to reproduce the experimental $F_h$ (activity started in the course of the 2022-2023 ITER internship program). In particular, the already implemented reduced MHD coupling with the STARWALL code \cite{merkel2015linear} \cite{hoelzl2012coupling} has been extended to full MHD. \medskip

In the future, the above-mentioned halo current coupling can be well implemented building on the full MHD eddy current model; however, because of the substantial differences lying in the electric and magnetic fields' treatment and the coupling equations themselves, this step might not be necessary right away, especially if the reduced MHD model already succeeds in attaining a faithful reproduction of the experimental findings.
\subsection*{Tasks and deliverables for Year 1}
The pre-set deliverables in the Implementing Agreement (IA) for Year 1 are here reported:
\begin{enumerate}
\item \textbf{Investigate the generalization of the JOREK boundary conditions}
\begin{enumerate}
\item 	Explore general boundary conditions that relax the $\Delta I_p$ constraint and investigate their implementation.
\item 	Verify the new boundary conditions and their implementation by constructing a test case in which $\Delta I_p \neq 0$ is enforced in JOREK through prescribed source/sink currents.
\item Formalize the strategy for the general halo current coupling with resistive wall codes (CARIDDI and/or STARWALL) in the reduced and full MHD models.
\end{enumerate}
\item \textbf{General halo current coupling with resistive wall codes (CARIDDI and/or STARWALL)}
\begin{enumerate}
\item Develop a current conserving mapping between the JOREK boundary and the resistive wall code representations for the plasma-facing components (PFC).
\item Implement and test the general coupling scheme developed in 1c in the reduced MHD model and the resistive wall codes using the mapping developed in 2a
\end{enumerate}
\end{enumerate}
\textbf{D1:} \begin{itemize}
\item Report on Year 1 activities;
\item Complete subtasks 1.a, 1.b, 1.c;
\item Begin subtasks 2.a and 2.b.
\end{itemize}

\medskip
Compared to the above deliverables, a slight deviation has been agreed with the IO RO in the course of the Year 1 activities, in particular:
\begin{itemize}
\item 1(a) and 1(c) have been successfully completed;
\item Tests were extensively made only on the CARIDDI side, deepening the aspects of point 1(c);
\item The activities entailed by 1(b), requiring an adaptation of the \texttt{CARIDDI\_J} code version as a pre-requisite, have been started and are on the way towards the first \texttt{JOREK-CARIDDI} tests with coupled halo currents.
\item 2(a) and 2(b) are on the way as expected.
\end{itemize}
\subsection*{Content}

This report deals with the results obtained from the activities carried out in the course of Year 1 of the PhD contract (2023-2024). 

\medskip
Chapter \ref{Full MHD} deals with the full MHD extension and the main results of the validation activity.

\medskip
Chapter \ref{Halo} features the theoretical background, model formulation, solution methods and simulation results leading up to the consistency assessment of the halo current coupling scheme for the relaxation of the $I_p$-symmetry. \\
Next steps in view of the first JOREK-CARIDDI coupling test cases are discussed.