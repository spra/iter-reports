\contentsline {chapter}{\numberline {1}Introduction}{2}{chapter.1}%
\contentsline {section}{\numberline {1.1}Halo current coupling}{2}{section.1.1}%
\contentsline {section}{\numberline {1.2}Methods for the solution of the conduction/induction problem}{2}{section.1.2}%
\contentsline {subsection}{\numberline {1.2.1}The "staggered" approach}{2}{subsection.1.2.1}%
\contentsline {subsection}{\numberline {1.2.2}Inductance-based approach}{3}{subsection.1.2.2}%
\contentsline {section}{\numberline {1.3}Evaluation of wall forces with Full MHD field}{3}{section.1.3}%
\contentsline {section}{\numberline {1.4}Running Full MHD with multiple free boundary harmonics}{3}{section.1.4}%
\contentsline {subsection}{\numberline {1.4.1}Tearing Mode 0,1}{3}{subsection.1.4.1}%
